//
// Created by ScanViz on 11.03.2021.
//
#pragma once
#include <memory>
#include <Player.hpp>

namespace game
{
    using PlayerPtr = std::shared_ptr<Player>;

    class Game {
    public:
        void Run();
    private:
        void AskRepeat();
        void EndGame();

    private:
        bool gameOn = true;
        std::array<PlayerPtr,2> players = {nullptr,nullptr};
    };
}


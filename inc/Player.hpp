//
// Created by ScanViz on 11.03.2021.
//
#pragma once
#include <memory>
#include <Character.hpp>

namespace game
{
    class Player {
        public:
            Player() = default;
            ~Player() = default;
            void SetChar(std::shared_ptr<Character> ch);
        private:
            std::shared_ptr<Character> ch = nullptr;
    };
}


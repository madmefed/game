//
// Created by ScanViz on 11.03.2021.
//
#include <Game.hpp>

int main(int argc, char** argv)
{
    game::Game game;
    game.Run();
    return 0;
}

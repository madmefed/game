//
// Created by ScanViz on 11.03.2021.
//

#include "../inc/Game.hpp"
#include <iostream>

namespace game
{

    void Game::Run()
    {
        while (gameOn)
        {
            ChooseCharecter();
            Battle();
            AskRepeat();
        }
    }

    void game::Game::EndGame() {
        gameOn = false;
    }

    void Game::AskRepeat() {

        char ans;
        std::cerr << "Another round? (y/n)" << "\n";
        std::cin >> ans;

        if(ans == 'n')
            EndGame();
    }
}